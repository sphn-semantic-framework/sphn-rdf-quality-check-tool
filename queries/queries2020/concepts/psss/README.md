# SPARQL queries for PSSS data set 

Use the following icons to complete 

* :white_check_mark: Query run sucessfully. Results of the query execution are coherent with the source data. Cross validation performed with business people.
* :construction_worker: Query run sucessfully. QC work started. Result are not as expected or some are a bit wrong.... Open questions with business. Work is in progress.
* :question: Query run sucessfully. Result are not as expected with other hospitals or ontology. Open questions for the sphn working group.
* :1234: Query run sucessfully. QC / validation of the data with business was not yet performed.
* :zero: Query run but no results are available.
* :x: Query is not working in the dataset or gives error.


Check also the core queries in here:
* https://git.dcc.sib.swiss/hospital-it/sphn-quality-assurance/-/blob/master/queries/core/README.md

## PSSS queries  

| Query / concept                                                           | Available from        | HUG (Geneva)          | CHUV (Lausanne)       | INSEL (Berne)         | Basel                 | USZ (Zurich)          | Related issues                           |
| ------------------------------------------------------------------------- | --------------------- | --------------------- | --------------------- | --------------------- | --------------------- | --------------------- |----------------------------------------- |
| [Encounter.rq](Encounter.rq)                                              | 03/2020               | :1234:                |                       |                       |                       |                       |                                          |
| [FophProcedure.rq](FophProcedure.rq)                                      | 07/2020               |                       |                       |                       |                       |                       |                                          |
| [FractionOfInspiratoryOxygen.rq](FractionOfInspiratoryOxygen.rq)          | 03/2020               | :1234:                |                       |                       |                       |                       |                                          |
| [InfectionSuspected.rq](InfectionSuspected.rq)                            | 03/2020               | :1234:                |                       |                       |                       |                       |                                          |
| [Movement.rq](Movement.rq)                                                | 03/2020               | :1234:                |                       |                       |                       |                       |                                          |
| [OxygenSaturation.rq](OxygenSaturation.rq)                                | 03/2020               | :1234:                |                       |                       |                       |                       |                                          |
| [UrineOutput.rq](UrineOutput.rq)                                          | 03/2020               | :1234:                |                       |                       |                       |                       |                                          |



## Limit for subject pseudo identifiers


For all queries, you can limit the results for one or several patient by simply filtering on the corresponding pseudo identifier by adding the following piece of code after the `where {`

```sql
... * where { 
    FILTER(?patient_pseudoid IN ("PSEUDO_IDENTIFIER"))
```
Use the query [SubjectPseudoIdentifier.rq](https://git.dcc.sib.swiss/hospital-it/sphn-quality-assurance/-/blob/master/queries/core/SubjectPseudoIdentifier.rq) to get the list of identifiers.

or for multiple subjects

```sql
... * where { 
    FILTER(?patient_pseudoid IN ("PSEUDO_IDENTIFIER_1", "PSEUDO_IDENTIFIER_2"))
```
```
