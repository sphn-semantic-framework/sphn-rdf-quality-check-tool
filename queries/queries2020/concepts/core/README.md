# SPARQL queries for SPHN core data set 

Use the following icons to complete

* :white_check_mark: Query run sucessfully. Results of the query execution are coherent with the source data. Cross validation performed with business people.
* :construction_worker: Query run sucessfully. QC work started. Result are not as expected or some are a bit wrong.... Open questions with business. Work is in progress.
* :question: Query run sucessfully. Result are not as expected with other hospitals or ontology. Open questions for the sphn working group.
* :1234: Query run sucessfully. QC / validation of the data with business was not yet performed.
* :zero: Query run but no results are available.
* :x: Query is not working in the dataset or gives error.

## Core queries  

| Query / core concept                                                      | HUG (Geneva)          | CHUV (Lausanne)       | INSEL (Berne)         | Basel                 | USZ (Zurich)          | Related issues                           |
| ------------------------------------------------------------------------- | --------------------- | --------------------- | --------------------- | --------------------- | --------------------- | ---------------------------------------- |
| [AdministrativeGender.rq](AdministrativeGender.rq)                        | :question:            |                       |                       |                       |                       | [issue 5](../../issues/5)                |
| [BirthDatetime.rq](BirthDatetime.rq)                                      | :question:            |                       |                       |                       |                       | [issue 5](../../issues/5)                |
| [BirthDatetimeAndAge.rq](BirthDatetimeAndAge.rq)                          | :1234:                |                       |                       |                       |                       |                                          |
| [CivilStatus.rq](CivilStatus.rq)                                          | :question:            |                       |                       |                       |                       | [issue 5](../../issues/5)                |
| [Consent.rq](Consent.rq)                                                  | :1234:                |                       |                       |                       |                       |                                          |
| [DeathStatus.rq](DeathStatus.rq)                                          | :question:            |                       |                       |                       |                       | [issue 5](../../issues/5)                |
| [DrugAdminstrationEvent.rq](DrugAdminstrationEvent.rq)                    | :1234:                |                       |                       |                       |                       |                                          |
| [Encounter.rq](Encounter.rq)                                              | :1234:                |                       |                       |                       |                       |                                          |
| [FophDiagnosis.rq](FophDiagnosis.rq)                                      | :1234:                |                       |                       |                       |                       |                                          |
| [FophProcedure.rq](FophProcedure.rq)                                      | :zero:                |                       |                       |                       |                       |                                          |
| [LabResult.rq](LabResult.rq)                                              | :1234:                |                       |                       |                       |                       |                                          |
| [SimpleScore.rq](SimpleScore.rq)                                          | :1234:                |                       |                       |                       |                       |                                          |
| [SubjectPseudoIdentifier.rq](SubjectPseudoIdentifier.rq)                  | :white_check_mark:    |                       |                       |                       |                       |                                          |
| [SystemicArterialBloodPressure.rq](SystemicArterialBloodPressure.rq)      | :1234:                |                       |                       |                       |                       |                                          |



## Limit for subject pseudo identifiers


For all queries, you can limit the results for one or several patient by simply filtering on the corresponding pseudo identifier by adding the following piece of code after the `where {`

```sql
... * where { 
    FILTER(?patient_pseudoid IN ("PSEUDO_IDENTIFIER"))
```
Use the query [SubjectPseudoIdentifier.rq](https://git.dcc.sib.swiss/hospital-it/sphn-quality-assurance/-/blob/master/queries/core/SubjectPseudoIdentifier.rq) to get the list of identifiers.

or for multiple subjects

```sql
... * where { 
    FILTER(?patient_pseudoid IN ("PSEUDO_IDENTIFIER_1", "PSEUDO_IDENTIFIER_2"))
```
