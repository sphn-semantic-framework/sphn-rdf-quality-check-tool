import requests
import sys

query_file_name = sys.argv[2]
sparql_endpoint = sys.argv[1]
 
def exec_sparql(query_file):
    query = open(query_file, "r").read()
    return requests.post(sparql_endpoint, data = {'query' : query}, timeout=None)
 
response = exec_sparql(query_file_name).json()
 
columns = response["head"]["vars"]
rows = response["results"]["bindings"]

headers = ""
for c in columns:
    headers += c + "\t"
print(headers)

for b in rows:
    formatted_row = ""
    for c in columns:
        value=c+"_undefined"
        if(c in b):
            value = b[c]["value"].replace("http://sphn.ch/rdf/resource/", "resource:")
        formatted_row += value + "\t"
    print(formatted_row)

