## Benchmark

To run a query, specify the endpoint and the query file name

```
time python3 sparql-query.py http://localhost:3030/psss-202007 count-triplets.rq > result.tsv
```
