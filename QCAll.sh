folder=../sphn-rdf-hackathon/
jvmoptions=-Xmx2g 
echo "running for HUG ... "
java -jar $jvmoptions sphn-quality-check.jar ./hospital-properties/QCHUG.properties > $folder/report.hug.log
echo "running for CHUV ... "
java -jar $jvmoptions sphn-quality-check.jar ./hospital-properties/QCCHUV.properties > $folder/report.chuv.log
echo "running for USZ ..."
java -jar $jvmoptions sphn-quality-check.jar ./hospital-properties/QCUSZ.properties > $folder/report.usz.log
echo "running for berne ..."
java -jar $jvmoptions sphn-quality-check.jar ./hospital-properties/QCUSB.properties > $folder/report.usb.log
echo "running for INSEL ..."
java -jar $jvmoptions sphn-quality-check.jar ./hospital-properties/QCINSEL.properties > $folder/report.insel.log

echo "running for GLOBAL"
java -jar $jvmoptions sphn-quality-check.jar ./hospital-properties/QCGLOBAL.properties > $folder/report.global.log
