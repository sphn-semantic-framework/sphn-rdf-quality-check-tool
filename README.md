# SPHN RDF Quality Check Tool - QC tool

The SPHN network's hospitals use a variety of database systems to store their clinical data, which can be found in many formats such as a structured database SQL or non-structured non-SQL format. Data quality validation and assurance has been a difficult task to handle with such large amounts of data, and it is sometimes regarded as a time-consuming and resource-intensive task, delaying the work that needs to be done by the researchers. Thus, using RDF standard format and constraints with semantic web tools, is thought to address the previous challenges if it is accompanied by a scenario-based multidimensional data quality checks, which can be run automatically against the hospital's RDF data to ensure adherence to the SPHN schema and validity against the specific criteria to be used as expected.

SPHN RDF Quality Check tool currently supports the following data quality checks: 

* SHACL constraints against the RDF schema of a specific project, checking for RDF schema compliance and constraint validity.
* Several SPARQL queries to evaluate the completeness and validity of the data. Furthermore, to provide dataset profiling tables that assist researchers in comprehending descriptive statistics about the data available and ensuring that the results are as expected.

## QC Tool setups and requirements: 

Because the tool is delivered as a ready-to-run Java jar file, it can be used on any OS that supports Java. There are no transaction size limits: bulk uploads into a TDB2 can be in the hundreds of millions of triples, depending on disc storage. The RDF data quality checks, on the other hand, were tested with the following specifications as the minimum software and hardware requirements for the tool to run smoothly and error-free:

- Apache Jena4 with TDB2 requires: **Java JDK 11**
- Java Virtual Machine JVM with memory allocation minimum as **4 GB**. 
- Disk storage equal to the size of the **original data x 2.5**

## QC Tool Configurations

The QC tool designed to read the configuration parameters defined by the user in a ".properties" file. The default config file is [QC.properties](/QC.properties)

The General  Parameters are the following: 

- The directory where the ontology files are stored, by default it is referring to [ontology2022](/ontologies/ontology2022) which contains the SPHN 2022 ontology pre-loaded as part of the repository files
```
ontology.dir=ontologies/ontology2022 
```
- The directory where the shape files (the SHACL shapes (constraints)) are stored, by default it is referring to [shapes2022](/shapes/shapes2022) with specific shapes compatible with SPHN 2022 ontology
```
shapes.dir=shapes/shapes2022
```
- The directory where the statistics and quality validation SPARQL queries are stored, by default it is referring to [queries2022](/queries/queries2022)
```
queries.dir=queries/queries2022
```
- QC tool generated Report layout, by default set to short, if it is set to "true" a long file `shacl_full_report.ttl` will be generated
```
shapes.logfulldetails=false
```
- The limit number of rows to display for flat representation of concepts in the report.log
```
sample.queries.size: 
```
Based on the specific parameters related to point where the data is stored in the QC.properties files the QC tool can run in 2 modes as the following:
 
- (Default) Local: Ontology and Resources will be loaded into the hosting machine file system, consuming the storage instead of memory, with TDB2 Jena RDF engine. This is the default mode to run the tool against a big data file with limited memory and I/O resources. 
The local mode is especially useful for performing quality checks directly on the data hosting machine; the tool should be installed on the data storage machine. The Apache Jena TDB2 engine used in the tool backend currently allows the tool to run quality checks validation against a full dataset containing hundreds of millions of triples.

Related configuration parameters: 

```
resources.dir=data/test_data
```
- (Not supported) ~~Remote (the QC will run against a SPARQL endpoint where the resources and ontology was previously loaded)
The remote mode is useful to perform a QC on the full dataset without the need to deploy the tool on the data hosting machine.~~

```
sparql.endpoint: The remote HTTP SPARQL endpoint
```

**The QC tool can't be run with the moded specific configuration parameters active for both modes, therefore "sparql.endpoint" should be always disabled (commented) when the run in local mode since the remote mood is not supported in V.2**

- resources for external ontologies as SNOMED, LOINC, UCUM, ... used in SPHN 2022

```
external_resources.dir=external-resources
```
- manage external ontologies loading and the shacl validation library

```
driver_project.is2020=false
```

## QC tool Usage
Run the following command (will use by default QC.properties)
```bash
java -jar sphn-quality-check-tool.jar
```

Or use a personal config file and limit the allocated memory to 4GB for the Java virtual machine if necessary:
```bash
java -jar -Xmx4g sphn-quality-check-tool.jar HUG-PSSS-QC.properties
```
The output of the QC tool will be generated on the terminal directly with a template as the following [report.log](report.log.template), the report contains the results of the queries, then the SHAPES contraints validation. 

If the user want to store the output report as a txt file, just add the following to the command before running the tool
```bash
java -jar sphn-quality-check-tool.jar > report_output.txt
```

## Export (Under development)

Here is an example:

Add in your QC.properties 
* export.dir: The directory where to export "flat files" per concept
* export.format can be: TSV, JSON, CSV, XML
* export.generatedFromConceptAttributes defines if
    * true: the data is generated automatically from the concepts and attributes found in the RDF data
    * false: based on the SPARQL queries in [concepts](queries/concepts/core) and specific driver project from `driver_project.dir` 
* driver_project.dir can be: psss sfnr spo shfn (load ontology, SHACL, queries for a specific driver project)
* mapping.file for reidentification, see [Reidentification](##Reidentification)
* resources.maxSize for the max size of byte from files that the application can use to generate the QC
  * 0 or absent for no limits
  * if resources.maxSize present, shuffle files order to sample data

```
export.dir=export
export.format=TSV

# specific queries/shacl to a project: psss sfnr spo shfn ...
driver_project.dir=sfnr

# mapping file to reidentify the data extracted from RDF
mapping.file=mapping_ids.csv
```

## Reidentification (Under development)

If export is CSV, and if the option `mapping.file` is passed in the properties file, reidentification will be made if possible (possible crash)

Format of the mapping file is csv (separated with ,), as follow:

| id  |  anoId | type   |  shift |
|---|---|---|---|
| 1  | fsfsfss  | patient  | 14  |

The reidentification will, in the previously generated csv :

* replace the anoId with the id for columns patient_pseudoid, L2_subject_pseudo_identifier, encounter_pseudoid & "L3_encounter in CSV files
* unshift the dates based on patient id

## License and Copyright

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics

The SPHN Quality Check tool is licensed under the GPLv3 (see [LICENSE](LICENSE)).

## Contributors 

The SPHN Quality Check tool is co-developed by SIB (Swiss Institute of Bioinformatics) and HUG (Geneva University Hospitals).

## Contact

For any question or comment, please contact the data coordination center at fair-data-team@sib.swiss.

[//]: # " ##ToDo: * Download ontology on the fly instead of copying it here."
[//]: # " * Add a SPARQL query verifying attributes absent in present concepts"
[//]: # " * Add more RDF quality matrixes and layers. ref: https://sansa-stack.net/quality-assessment-of-rdf-datasets-at-scale/"
