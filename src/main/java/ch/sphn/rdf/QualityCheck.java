package ch.sphn.rdf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.utils.DateUtils;
import org.apache.jena.query.ARQ;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.shacl.ShaclValidator;
import org.apache.jena.shacl.ValidationReport;
import org.apache.jena.sparql.resultset.ResultsFormat;
import org.apache.jena.tdb2.TDB2Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.topbraid.shacl.validation.ValidationUtil;

/**
 * @author ddii / pdth 
 * 02.12.2019
 */
public class QualityCheck implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(QualityCheck.class);
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    private final String exportDir;
    private final List<String> ontologyFileNames;
    private final List<String> qcQueriesFileNames;
    private final List<String> shapesFileNames;
    private final List<String> resourceFileNames;
    private final Long startTime;
    private final String sparqlEndpoint;
    private final Boolean isRemote;
    private final int sampleQueriesSize;
    private final BufferedWriter writer;
    private final ExportFormat format;
    private final QCConfig config;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public QualityCheck(QCConfig config) throws IOException {

        this.config = config;
        this.ontologyFileNames = config.getOntologyFileNames();
        if (!config.isSPARQLEndpoint()) {
            this.resourceFileNames = config.getResourceFileNames();
            this.sparqlEndpoint = null;
            this.isRemote = false;
        } else {
            this.isRemote = true;
            this.sparqlEndpoint = config.getSPARQLEndpoint();
            this.resourceFileNames = null;
        }
        this.qcQueriesFileNames = config.getQCQueriesFileNames();
        this.shapesFileNames = config.getShapesFileNames();
        this.sampleQueriesSize = config.getSampleQueriesSize();
        String reportFileName = config.getReportFileName();
        this.startTime = config.getStartTime();
        this.exportDir = config.getExportDir();
        this.format = ExportFormat.valueOf(config.getExportFormat());

        log("Reading " + qcQueriesFileNames.size() + " QC queries " + config.getQcQueriesDir());
        this.writer = Files.newBufferedWriter(Paths.get(reportFileName));
    }

    public static void main(String[] args) {

        String configFile = (args != null && args.length > 0)  ?  args[0] : "QC.properties" ;
        try (QualityCheck qc = new QualityCheck(new QCConfig(configFile))) {
            qc.doQualityCheck();
        } catch (Exception e) {
            LOGGER.error("Exception running QC", e);
            System.exit(-1);
        }
        System.exit(0);
    }

    private static Date addDays(Date d, int days) {
        Calendar cal = new GregorianCalendar();
        try {
            cal.setTime(d);
            cal.add(Calendar.DAY_OF_WEEK, days);
            return cal.getTime();
        } catch (NullPointerException e) {
            return d;
        }
    }

    private void parseResources(Model model) {
        log("Reading resources from " + resourceFileNames.size());
        long bytesRead = 0;
        if (config.getResourcesMaxSize() != BigInteger.ZERO) {
            log(">> shuffling resource files <<");
            Collections.shuffle(resourceFileNames); // shuffle files to sample data
        }
        for (String resourceFile : resourceFileNames) {
            bytesRead += addFileToModel(resourceFile, model);
            if (config.getResourcesMaxSize().compareTo(BigInteger.valueOf(bytesRead)) < 0 && !Objects.equals(config.getResourcesMaxSize(), BigInteger.ZERO)) {
                log("max bytes reached from files: " + formatBytes(bytesRead) + " / " + config.getResourcesMaxSize());
                return;
            }
        }
    }

    private String getContentFromFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(new File(fileName).toPath()));
    }

    private void parseOntology(Model model) {
        log("Reading ontology from " + ontologyFileNames.size());
        for (String ontologyFileName : ontologyFileNames) {
            addFileToModel(ontologyFileName, model);
        }
    }

    private String formatBytes(Long bytes) {
        if (bytes > 1024 * 1024) {
            return bytes / (1024 * 1024) + "Mb";
        } else if (bytes > 1024) {
            return bytes / 1024 + "Kb";
        } else {
            return bytes + "b";
        }
    }

    private long addFileToModel(String filename, Model model) {
        try {
            long length = new File(filename).length();
            log("Adding " + filename + " > " + formatBytes(length));
            model.read(new FileInputStream(filename), null, "TURTLE");
            return length;
        } catch (Exception e) {
            log("\tFailed to read " + filename + "\r\n\t> Error message: " + e.getMessage());
            LOGGER.error("Error reading file", e);
            return 0L;
        }
    }

    private void runQCQueries(Model model) throws IOException {
        for (String fileName : this.qcQueriesFileNames) {
            String query = getContentFromFile(fileName);
            String description = Arrays.stream(query.split("\n")).filter(qc -> qc.startsWith("#")).collect(Collectors.joining("\n"));
            log(description);
            executeQuery(fileName, query, model);
        }
    }

    private void executeQuery(String fileName, String query, Model model) {
        this.executeQuery(fileName, query, model, null);
    }

    private void executeQuery(String fileName, String query, Model model, ResultsFormat resultsFormat) {
        long start = System.currentTimeMillis();
        ResultSet resultSet;
        try (QueryExecution queryExecution = this.isRemote ?
                QueryExecutionFactory.sparqlService(this.sparqlEndpoint, query) :
                QueryExecutionFactory.create(query, model)) {
            log("Executing " + fileName + (this.isRemote ? " against remote endpoint " + this.sparqlEndpoint : " against resources files"));
            resultSet = queryExecution.execSelect();
            OutputStream out;
            if (resultsFormat == null) {
                out = new ByteArrayOutputStream();
                ResultSetFormatter.out(out, resultSet, model);
                log(out.toString());
            } else {
                out = new FileOutputStream(this.exportDir + File.separator + fileName);
                ResultSetFormatter.output(out, resultSet, resultsFormat);
            }
            log(formatElapsedTime(System.currentTimeMillis() - start));
        } catch (Exception e) {
            log("Failed to execute query for  " + query + " after " + (System.currentTimeMillis() - start) + " ms" + "\r\n\t cause " + e.getCause());
            LOGGER.error("executeQuery failed for {}", query, e);
        }
    }

    public String getStringAfterLastSlash(String str) {
        int index = str.lastIndexOf('/');
        return (str.substring(index + 1));
    }

    private String formatElapsedTime(long elapsedTime) {
        long elapsedTimeInSeconds = elapsedTime / 1000;
        if (elapsedTimeInSeconds < 10) { // up to 10 seconds we show in ms
            return "(" + elapsedTime + " ms)";
        } else if (elapsedTimeInSeconds < 60 * 60) { // up to 1 hour we show in seconds
            return "(" + elapsedTimeInSeconds + " seconds)";
        } else
            return "(" + (elapsedTime / 1000 / 60) + " minutes)"; //then we show in minutes
    }

    private Map<String, List<String>> getConceptAttributes(Model model) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("QC00031-shows-ontology-classes-and-attributes.rq");
        assert is != null;
        InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);
        String query = reader.lines().collect(Collectors.joining("\n"));
        ResultSet resultSet;
        try (QueryExecution queryExecution = QueryExecutionFactory.create(query, model)) {
            resultSet = queryExecution.execSelect();
        }
        Map<String, List<String>> conceptAttributes = new HashMap<>();
        while (resultSet.hasNext()) {
            QuerySolution solution = resultSet.next();
            String concept = solution.get("concept").asResource().getURI();
            String attribute = solution.get("attribute").asResource().getURI();
            conceptAttributes.computeIfAbsent(concept, x -> conceptAttributes.put(concept, new ArrayList<>()));
            conceptAttributes.get(concept).add(attribute);
        }

        return conceptAttributes;
    }

    private void runShapeConstraints(Model model) throws IOException {
        String ontology = "tmp/ontology";
        if(Files.exists(Paths.get(ontology))){
            FileUtils.deleteDirectory(new File(ontology));
        }
        //Create RDF storage for the shape files in the file system using TDB2Factory, https://jena.apache.org/documentation/javadoc/tdb2/org/apache/jena/tdb2/TDB2Factory.html#connectDataset(java.lang.String)
        Dataset dataset = TDB2Factory.connectDataset(ontology);
        dataset.begin(ReadWrite.WRITE);
        // Get model inside the transaction
        Model shapeModel = dataset.getDefaultModel();

        shapeModel.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
        shapeModel.setNsPrefix("sphn", config.isDriverProjectIs2020() ? "http://sphn.ch/rdf" : "https://biomedit.ch/rdf/sphn-resource/");
        shapeModel.setNsPrefix("sh", "http://www.w3.org/ns/shacl#");

        log("Reading shape from " + this.shapesFileNames.size());
        for (String shapeFile : shapesFileNames) {
            addFileToModel(shapeFile, shapeModel);
        }

        log("Validating SHACL shapes ................ ");
        String distinctShapeQuery = "PREFIX sh:<http://www.w3.org/ns/shacl#> select distinct ?shape where { ?shape a sh:NodeShape. }";
        executeQuery("distinctShapeQuery", distinctShapeQuery, shapeModel);

        Model reportModel;
        if (config.isDriverProjectIs2020()) {
            // Validates with SHACL
            Resource report = ValidationUtil.validateModel(model, shapeModel, true);
            reportModel = report.getModel();
        } else {
            log("Loading external resources as model is 2021 format");
            for (String resourceFile : config.getExternalResourceFileNames()) {
                addFileToModel(resourceFile, model);
            }
            // validate
            ValidationReport report = ShaclValidator.get().validate(shapeModel.getGraph(), model.getGraph());
            reportModel = report.getModel();
        }
        String distinctViolationsQueryPerPath = "PREFIX sh:<http://www.w3.org/ns/shacl#> " + //
                "PREFIX sphn:" + (config.isDriverProjectIs2020() ? "<http://sphn.ch/rdf> " : "<https://biomedit.ch/rdf/sphn-resource/> ")
                + "SELECT ?resultSeverity ?resultPath ?sourceConstraintComponent ?sourceShape "//
                + "(COUNT(?node) AS ?count) "//
                + "(MIN(?resultMessage) AS ?resultMessageMin) (MIN(?focusNode) AS ?focusNodeMin) (MIN(?value) AS ?valueMin) " //
                + "WHERE { "//
                + "?_ sh:result ?node . "//
                + "OPTIONAL { ?node sh:focusNode ?focusNode . } "//
                + "OPTIONAL { ?node sh:resultMessage ?resultMessage . } "//
                + "OPTIONAL { ?node sh:resultPath ?resultPath . } "//
                + "OPTIONAL { ?node sh:resultSeverity ?resultSeverity. } "//
                + "OPTIONAL { ?node sh:sourceConstraintComponent ?sourceConstraintComponent. } "//
                + "OPTIONAL { ?node sh:sourceShape ?sourceShape. } "//
                + "OPTIONAL { ?node sh:value ?value. } "//
                + "} GROUP BY ?resultPath ?resultSeverity ?sourceConstraintComponent ?sourceShape ORDER BY ?resultSeverity ?resultMessage";
        executeQuery("the distinct SHACL Violations Per resultPath", distinctViolationsQueryPerPath, reportModel);
        //Log only full details if specified
        if (config.logShaclFullDetails()) {
            RDFDataMgr.write(new FileOutputStream("shacl_full_report.ttl"), reportModel, RDFFormat.TURTLE_PRETTY);
        }
        dataset.commit();
        dataset.end();
        dataset.close();
    }

    public void exportDataInFormat(Model model, ExportFormat format) throws IOException {
        if (this.isRemote) {
            log("For the moment export Data can only be executed against local resources. Change to resource.dir instead of sparql.endpoint.");
            return;
        }
        File exportDirFile = new File(this.exportDir);
        if (!exportDirFile.exists() && exportDirFile.mkdirs()) {
            log("Export folder created");
        }
        for (File file : Objects.requireNonNull(exportDirFile.listFiles())) {
            Files.delete(file.toPath());
        }
        ResultsFormat resultsFormat;
        switch (format) {
        case CSV:
            resultsFormat = ResultsFormat.FMT_RS_CSV;
            break;
        case XML:
            resultsFormat = ResultsFormat.FMT_RDF_XML;
            break;
        case JSON:
            resultsFormat = ResultsFormat.FMT_RS_JSON;
            break;
        default:
            resultsFormat = ResultsFormat.FMT_RS_TSV;
        }

        if (this.config.isGeneratedFromConceptAttributes()) {
            this.exportGeneratedQueries(model, resultsFormat);
        } else {
            this.exportSPARQLQueries(model, resultsFormat);
        }

        if (config.getMappingFile() != null && ExportFormat.CSV.equals(this.format)) {
            log("Start de-anonymization with " + config.getMappingFile());
            this.deAnonymize(config.getMappingFile());
        } else {
            log("No de-anonymization needed.");
        }
    }

    private void exportSPARQLQueries(Model model, ResultsFormat format) throws IOException {
        File coreDir = new File(this.config.getQcQueriesDir() + File.separator + "concepts" + File.separator + "core");
        if (coreDir.exists()) {
            for (File file : Objects.requireNonNull(coreDir.listFiles())) {
                if (!file.getName().contains(".rq")) {
                    continue;
                }
                this.executeQuery("core-" + file.getName().replace(".rq", ".csv"), getContentFromFile(file.getPath()), model, format);
            }
        }
        File driverDir = new File(this.config.getQcQueriesDir() + File.separator + "concepts" + File.separator + config.getDriverProjectDir());
        if (driverDir.exists()) {
            for (File file : Objects.requireNonNull(driverDir.listFiles())) {
                if (!file.getName().contains(".rq")) {
                    continue;
                }
                try (Stream<String> lines = Files.lines(file.toPath(), StandardCharsets.UTF_8)) {
                    lines.reduce((x, y) -> x + "\n" + y)
                            .ifPresent(q -> this.executeQuery(config.getDriverProjectDir() + "-" + file.getName().replace(".rq", ".csv"), q, model, format));
                }
            }
        }
    }

    private void exportGeneratedQueries(Model model, ResultsFormat resultsFormat) {
        Map<String, List<String>> conceptAttributes = getConceptAttributes(model);
        for (Map.Entry<String, List<String>> entry : conceptAttributes.entrySet()) {
            String queryGenerated = buildQueryDynamicallyForConcept(entry.getKey(), entry.getValue());
            this.executeQuery(entry.getKey(), queryGenerated, model, resultsFormat);
        }
    }

    private void deAnonymize(String inputFilePath) throws IOException {
        Map<String, Mapping> mapping;
        try {
            File inputF = new File(inputFilePath);
            InputStream inputFS = new FileInputStream(inputF);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            // skip the header of the csv
            mapping = br.lines().skip(1).map(line -> new Mapping(line.split(";"))).collect(Collectors.toMap(m -> m.ano, m -> m));
            br.close();
        } catch (IOException e) {
            LOGGER.error("could not read mapping file", e);
            return;
        }
        for (File file : Objects.requireNonNull(new File(this.exportDir).listFiles())) {
            try (Reader in = new FileReader(file.getPath())) {
                try (PrintWriter pw = new PrintWriter(this.exportDir + File.separator + "_" + file.getName())) {
                    CSVParser parser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
                    pw.println("\"" + String.join("\";\"", parser.getHeaderNames()) + "\"");  //header
                    parser.forEach(r -> pw.println(deAnonymizeRecord(r, mapping))); // data
                    parser.close();
                } catch (IOException e) {
                    log("could not deAnonymize " + file.getName() + ": " + e.getMessage());
                }
            }
            log("deAnonymize: " + file.getName());
            file.deleteOnExit();
        }
    }

    private String deAnonymizeRecord(CSVRecord r, Map<String, Mapping> mapping) {
        List<String> headers = r.getParser().getHeaderNames();
        int colPatient = IntStream.range(0, headers.size())
                .filter(i -> Arrays.asList("L2_subject_pseudo_identifier", "A110_SubjectPseudoIdentifier_value", "patient_pseudoid", "SubjectPseudoIdentifier")
                        .contains(headers.get(i))).findFirst().orElse(-1);
        Mapping mSubject =
                colPatient == -1 ? new Mapping(new String[] { "", "", "", "0" }) : mapping.get(r.get(colPatient).replaceAll("(.*SubjectPseudoIdentifier-)", ""));
        List<String> out = new ArrayList<>();
        for (int i = 0; i < headers.size(); i++) {
            String header = headers.get(i);
            if (r.get(i) == null) {
                out.add("");
            } else if (header.contains("date") && !r.get(i).equals("")) { // 1 deAnonymize dates
                Date d = DateUtils.parseDate(r.get(i), new String[] { "yyyy-MM-dd'T'HH:mm:ss" });
                if (d == null) {
                    out.add(r.get(i));
                } else {
                    out.add(df.format(addDays(d, Optional.ofNullable(mSubject).map(m -> m.shift).orElse(0))));
                }
            } else if (Arrays.asList("patient_pseudoid", "L2_subject_pseudo_identifier", "SubjectPseudoIdentifier")
                    .contains(header)) { // 2 deAnonymize L2_subject_pseudo_identifier
                out.add(Optional.ofNullable(mSubject).map(m -> m.id).orElse(r.get(i)));
            } else if (Arrays.asList("encounter_pseudoid", "L3_encounter", "AdministrativeCase").contains(header)) {
                out.add(Optional.ofNullable(mapping.get(r.get(i).replaceAll("(.*Encounter-)|AdministrativeCase", ""))).map(m -> m.id).orElse(r.get(i)));
            } else {
                out.add(r.get(i));
            }
        }
        return "\"" + String.join("\";\"", out) + "\"";
    }

    public void exportSampleResources(Model model) {
        Map<String, List<String>> conceptAttributes = getConceptAttributes(model);
        for (Map.Entry<String, List<String>> entry : conceptAttributes.entrySet()) {
            String queryGenerated = buildQueryDynamicallyForConcept(entry.getKey(), entry.getValue());
            queryGenerated += " LIMIT " + this.sampleQueriesSize + "\n\n";
            ResultSet resultSet;
            try (QueryExecution queryExecution = QueryExecutionFactory.create(queryGenerated, model)) {
                resultSet = queryExecution.execSelect();
            }
            OutputStream out = new ByteArrayOutputStream();
            if (resultSet.hasNext()) {
                log("Query for " + entry.getKey() + "\n" + queryGenerated);
                ResultSetFormatter.out(out, resultSet, model);
                log(out.toString());
            }
        }
    }

    private String buildQueryDynamicallyForConcept(String concept, List<String> attributes) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * where {\n\t?resource a <").append(concept).append("> .\n");
        attributes.sort(Comparator.naturalOrder());
        for (String attribute : attributes) {
            String variable = getStringAfterLastSlash(attribute).replaceAll("[^a-zA-Z0-9]", "_");
            sb.append("\toptional {?resource <").append(attribute).append("> ?").append(variable).append(" }\n");
        }
        sb.append("}");
        return sb.toString();
    }

    private void log(String msg) {
        if (this.writer != null) {
            try {
                this.writer.write(msg + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        LOGGER.info(msg);
    }

    @Override public void close() throws Exception {
        this.writer.close();
    }

    public void doQualityCheck() throws IOException {

        log("Starting at " + dtf.format(LocalDateTime.now()));
        String data = "tmp/data";
        //https://jena.apache.org/documentation/tdb/java_api.html
        if(Files.exists(Paths.get(data))){
            FileUtils.deleteDirectory(new File(data));
        }
        //Create RDF storage for the dataset with ontology in the file system using TDB2Factory, https://jena.apache.org/documentation/javadoc/tdb2/org/apache/jena/tdb2/TDB2Factory.html#connectDataset(java.lang.String)
        Dataset dataset = TDB2Factory.connectDataset(data) ;
        dataset.begin(ReadWrite.WRITE) ;
        // Get model inside the transaction
        Model model = dataset.getDefaultModel() ;

        //Model model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM).getBaseModel();
        ARQ.init();

        if (!this.isRemote) {
            this.parseOntology(model);
            this.parseResources(model);
            log("Mode: LOCAL");
            log("Creating a model ");
            log("Read in total (with ontology) " + model.size() + " triples in " + (System.currentTimeMillis() - this.startTime) + " ms");
        } else {
            log("Mode: REMOTE");
            log("Using remote SPARQL endpoint: " + this.sparqlEndpoint);
            log("Note that resources and the ontology must be loaded in the remote SPARQL endpoint beforehand.");
            log("This process will NOT load into a remote endpoint the ontology. The ontology.dir and resources.dir properties are ignored.");
        }

        //Running queries
        log("Running QC queries ... ");
        this.runQCQueries(model);

        if (this.sampleQueriesSize > 0) {
            log("Exporting samples of size " + this.sampleQueriesSize + " in the report");
            this.exportSampleResources(model);
        } else {
            log("sample.queries.size " + this.sampleQueriesSize + " is empty");
        }

        if (this.exportDir != null) {
            log("Start full export in " + this.exportDir + " in " + this.format);
            this.exportDataInFormat(model, this.format);
        } else {
            log("No full export is defined.");
        }

        //Running shape constraints
        if (!this.isRemote) {
            log("Running shape constraints against resources");
            this.runShapeConstraints(model);
        } else {
            log("For the moment shacl rules can only be executed against local resources. Change to resource.dir instead of sparql.endpoint.");
        }

        log("Finished at " + dtf.format(LocalDateTime.now()) + " in " + (System.currentTimeMillis() - this.startTime) / 1000 + " seconds");

        dataset.commit();

        log("Commiting in " + (System.currentTimeMillis() - this.startTime) / 1000 + " seconds");

        dataset.end() ;

        log("End in " + (System.currentTimeMillis() - this.startTime) / 1000 + " seconds");

    }

    static class Mapping {
        public final String id;
        public final String ano;
        public final String type;
        public final int shift;

        public Mapping(String[] array) {
            this.id = array[0];
            this.ano = array[1];
            this.type = array[2];
            this.shift = Integer.parseInt(array[3]);
        }
    }
}
