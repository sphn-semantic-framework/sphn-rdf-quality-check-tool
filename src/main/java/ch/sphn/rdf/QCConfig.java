package ch.sphn.rdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ddii
 * 02.12.2019
 */
public class QCConfig {
        private static final Logger LOGGER = LoggerFactory.getLogger(QCConfig.class);

        private final String shapesDir;
        private final String ontologyDir;
        private final String resourcesDir;
        private final String externalResourcesDir;
        private final String sparqlEndpoint;
        private final String qcQueriesDir;
        private final String driverProjectDir;
        private final String sampleQueriesSize;
        private final String exportDir;
        private final String exportFormat;
        private final String mappingFile;
        private final boolean logShaclFullDetails;
        private final boolean generatedFromConceptAttributes;
        private final long startTime;
        private final BigInteger resourcesMaxSize;
        private final boolean driverProjectIs2020;

        public QCConfig(String configFileName) {

                this.startTime = System.currentTimeMillis();
                try (InputStream input = new FileInputStream(configFileName)) {
                        Properties prop = new Properties();
                        prop.load(input);
                        this.ontologyDir = prop.getProperty("ontology.dir");
                        this.shapesDir = prop.getProperty("shapes.dir");
                        this.resourcesDir = prop.getProperty("resources.dir");
                        this.externalResourcesDir = prop.getProperty("external_resources.dir");
                        this.sparqlEndpoint = prop.getProperty("sparql.endpoint");
                        if (resourcesDir != null && resourcesDir.length() > 0 && sparqlEndpoint != null && sparqlEndpoint.length() > 0) {
                                throw new IllegalArgumentException("resources.dir and sparql.endpoint are mutually exclusive. Only one can be set");
                        }

                        this.qcQueriesDir = prop.getProperty("queries.dir");
                        this.sampleQueriesSize = prop.getProperty("sample.queries.size");
                        this.exportDir = prop.getProperty("export.dir");
                        this.exportFormat = prop.getProperty("export.format") != null ? prop.getProperty("export.format") : "TSV";
                        this.logShaclFullDetails = prop.getProperty("shapes.logfulldetails") != null && Boolean.parseBoolean(prop.getProperty("shapes.logfulldetails"));
                        this.generatedFromConceptAttributes = prop.getProperty("export.generatedFromConceptAttributes") != null && Boolean.parseBoolean(
                                prop.getProperty("export.generatedFromConceptAttributes"));
                        this.driverProjectDir = prop.getProperty("driver_project.dir");
                        this.mappingFile = prop.getProperty("mapping.file");
                        this.resourcesMaxSize = Optional.ofNullable(prop.getProperty("resources.maxSize")).map(BigInteger::new).orElse(BigInteger.ZERO);
                        this.driverProjectIs2020 = prop.getProperty("driver_project.is2020") != null && Boolean.parseBoolean(prop.getProperty("driver_project.is2020"));
                } catch (IOException e) {
                        throw new RuntimeException(e);
                }
        }

        private List<String> listFilesInFolderWithExtension(String folder, String extension) {
                try (Stream<Path> paths = Files.list(Paths.get(folder))) {
                        return paths.filter(q -> !Files.isDirectory(q))//
                                .map(f -> folder + File.separator + f.toFile().getName())//
                                .filter(f -> f.endsWith(extension))//
                                .collect(Collectors.toList());
                } catch (Exception e) {
                        LOGGER.warn("Files {}*{} do not load: {}", folder, extension, e.getMessage());
                        return Collections.emptyList();
                }
        }

        public String getReportFileName() {
                return "report.log.txt";
        }

        public int getSampleQueriesSize() {
                return (this.sampleQueriesSize != null) ? Integer.parseInt(this.sampleQueriesSize) : 0;
        }

        public boolean isSPARQLEndpoint() {
                return (this.sparqlEndpoint != null && this.sparqlEndpoint.startsWith("http"));
        }

        public List<String> getQCQueriesFileNames() {
                List<String> queries = listFilesInFolderWithExtension(this.qcQueriesDir, ".rq");
                queries.addAll(listFilesInFolderWithExtension(this.qcQueriesDir + File.separator + this.driverProjectDir, ".rq"));
                return queries;
        }

        public List<String> getResourceFileNames() {
                return listFilesInFolderWithExtension(this.resourcesDir, ".ttl");
        }

        public List<String> getExternalResourceFileNames() {
                return listFilesInFolderWithExtension(this.externalResourcesDir, ".ttl");
        }

        public List<String> getShapesFileNames() {
                List<String> shapes = listFilesInFolderWithExtension(this.shapesDir, ".ttl");
                shapes.addAll(listFilesInFolderWithExtension(this.shapesDir + File.separator + this.driverProjectDir, ".ttl"));
                return shapes;
        }

        public List<String> getOntologyFileNames() {
                List<String> ontologies = listFilesInFolderWithExtension(this.ontologyDir, ".ttl");
                ontologies.addAll(listFilesInFolderWithExtension(this.ontologyDir + File.separator + this.driverProjectDir, ".ttl"));
                return ontologies;
        }

        public String getExportFormat() {
                return this.exportFormat;
        }

        public String getQcQueriesDir() {
                return this.qcQueriesDir;
        }

        public String getExportDir() {
                return this.exportDir;
        }

        public String getMappingFile() {
                return this.mappingFile;
        }

        public String getSPARQLEndpoint() {
                return this.sparqlEndpoint;
        }

        public boolean logShaclFullDetails() {
                return this.logShaclFullDetails;
        }

        public Long getStartTime() {
                return this.startTime;
        }

        public boolean isGeneratedFromConceptAttributes() {
                return generatedFromConceptAttributes;
        }

        public String getDriverProjectDir() {
                return driverProjectDir;
        }

        public BigInteger getResourcesMaxSize() {
                return resourcesMaxSize;
        }

        public boolean isDriverProjectIs2020() {
                return driverProjectIs2020;
        }
}