package ch.sphn.rdf;

/**
 * ddii
 * 16.12.2019
 */
public enum ExportFormat {
    TSV,
    XML,
    CSV,
    JSON
}
